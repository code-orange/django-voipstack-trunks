import random
import string

from django.db import transaction
from django.conf import settings

from django_asterisk_models.django_asterisk_models.models import *
from django_voipstack_trunks.django_voipstack_trunks.models import *


def func_randompassword(pwlength):
    chars = string.ascii_uppercase + string.ascii_lowercase + string.digits
    return "".join(random.choice(chars) for x in range(pwlength))


def func_randomnumber(numlength):
    chars = string.digits
    chars = chars.replace("0", "")
    return "".join(random.choice(chars) for x in range(numlength))


def func_randomtrunk():
    while True:
        trunk_id = "sip" + func_randomnumber(6)

        try:
            find_endpoint = VoipTrunks.objects.get(trunk_name=trunk_id)
        except VoipTrunks.DoesNotExist:
            return trunk_id
        continue


def func_randompassword_default():
    return func_randompassword(16)


def func_add_trunk(tech, custnr):
    if tech == "sip":
        trunkid = func_randomtrunk()
        password = func_randompassword(16)

        with transaction.atomic():
            new_trunk = PsEndpoints(
                id=trunkid,
                transport="transport-udp",
                aors=trunkid,
                auth=trunkid,
                context="default",
                disallow="all",
                allow="g722,alaw,ulaw",
                direct_media="no",
                language="de",
                tone_zone="de",
                inband_progress="yes",
                disable_direct_media_on_nat="yes",
                dtmf_mode="auto",
                force_rport="yes",
                ice_support="yes",
                rewrite_contact="yes",
                rtp_symmetric="yes",
                send_diversion="yes",
                send_pai="yes",
                send_rpid="yes",
                msg100rel="yes",
                t38_udptl="no",
                t38_udptl_ec="redundancy",
                t38_udptl_maxdatagram=400,
                t38_udptl_nat="no",
                allow_transfer="no",
                accountcode=trunkid,
            )

            new_trunk.save(force_insert=True, using="asterisk_conf")

            new_cust_rel = CusCustnr(cusnr_sap=custnr, auth_num=0, trunk_name=new_trunk)

            new_cust_rel.save(force_insert=True, using="asterisk_conf")

            new_internal_rel = VoipTrunks(
                # TODO: Fix created_by
                customer=MdatCustomers.objects.get(
                    external_id=custnr, created_by=settings.MDAT_ROOT_CUSTOMER_ID
                ),
                tech_id=1,
                trunk_name=new_trunk.id,
                trunk_password=password,
                enabled=VoipTrunks.ENABLED,
            )

            new_internal_rel.save(force_insert=True)

            new_auth = PsAuths(
                id=trunkid, auth_type="userpass", password=password, username=trunkid
            )

            new_auth.save(force_insert=True, using="asterisk_conf")

            new_aor = PsAors(
                id=trunkid,
                max_contacts=1,
                remove_existing="yes",
                maximum_expiration=600,
            )

            new_aor.save(force_insert=True, using="asterisk_conf")

        return True

    return False
