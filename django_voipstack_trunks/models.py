from datetime import datetime

from django_mdat_customer.django_mdat_customer.models import *


class VoipTrunkTech(models.Model):
    name = models.CharField(max_length=30)
    prefix = models.CharField(max_length=10)

    def __str__(self):
        return self.name

    class Meta:
        db_table = "voip_trunk_tech"


class VoipTrunks(models.Model):
    # ENABLED_CHOICES
    ENABLED = 1
    DISABLED = 0

    ENABLED_CHOICES = (
        (ENABLED, "ENABLED"),
        (DISABLED, "DISABLED"),
    )

    # CDR_CHOICES
    CDR_NONE = 0
    CDR_ANON = 1
    CDR_FULL = 2

    CDR_CHOICES = (
        (CDR_NONE, "CDR_NONE"),
        (CDR_ANON, "CDR_ANON"),
        (CDR_FULL, "CDR_FULL"),
    )

    # USAGE_LIMIT_CHOICES
    NORMAL = 0
    SUSPENDED = 1

    USAGE_LIMIT_CHOICES = (
        (NORMAL, "NORMAL"),
        (SUSPENDED, "SUSPENDED"),
    )

    id = models.BigAutoField(primary_key=True)
    customer = models.ForeignKey(MdatCustomers, models.CASCADE, db_column="customer")
    tech = models.ForeignKey(VoipTrunkTech, models.CASCADE, db_column="tech")
    trunk_name = models.CharField(max_length=30)
    trunk_password = models.CharField(max_length=200)
    pwmgr_id = models.CharField(max_length=200, default=None, null=True)
    contract_id = models.CharField(max_length=200, default=None, null=True)
    added_date = models.DateTimeField(default=datetime.now)
    cdr_report = models.IntegerField(choices=CDR_CHOICES, default=CDR_NONE)
    cdr_report_email = models.EmailField(null=True)
    cdr_report_date = models.DateField(default=datetime.now)
    cdr_billed_until_date = models.DateField(default=datetime.now)
    enabled = models.IntegerField(choices=ENABLED_CHOICES, default=ENABLED)
    usage_limit = models.IntegerField(choices=USAGE_LIMIT_CHOICES, default=NORMAL)

    def __str__(self):
        return self.tech.name + " - " + self.trunk_name + " - " + self.customer.name

    class Meta:
        db_table = "voip_trunks"


class VoipTrunkAsteriskDb(models.Model):
    name = models.CharField(max_length=200, unique=True)
    db_ip = models.CharField(max_length=200)
    db_dbase = models.CharField(max_length=200)
    db_user = models.CharField(max_length=200)
    db_passwd = models.CharField(max_length=200)
    date_added = models.DateTimeField(default=datetime.now)

    class Meta:
        db_table = "voip_trunk_asterisk_db"
