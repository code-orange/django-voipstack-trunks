from django.core.management.base import BaseCommand

from dit_enterprisehub_sap.models import *
from dit_enterprisehub_ucmgmt.models import *


class Command(BaseCommand):
    help = "Sync call routing"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_customers = (
            DolTelCusNum.objects.using("sapserver")
            .order_by("u_custnr")
            .values_list("u_custnr", flat=True)
            .distinct()
        )

        for custnr in all_customers:
            trunk_entry_count = (
                CusCustnr.objects.using("mediasw_conf").filter(cusnr_sap=custnr).count()
            )

            customer = Ocrd.objects.using("sapserver").get(cardcode=custnr)

            if not trunk_entry_count:
                print(
                    "Trunk is missing an entry: "
                    + str(customer.cardname)
                    + " ("
                    + str(customer.cardcode)
                    + ")"
                )
                all_nums = DolTelCusNum.objects.using("sapserver").filter(
                    u_custnr=custnr
                )

                for number in all_nums:
                    print("Number: 0" + str(number.u_onkz) + str(number.u_mainnum))

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
