from django.core.management.base import BaseCommand

from django_voipstack_trunks.django_voipstack_trunks.func import func_add_trunk


class Command(BaseCommand):
    help = "Generate new trunk"

    def add_arguments(self, parser):
        parser.add_argument("tech", type=str)
        parser.add_argument("custnr", type=int)

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        tech = options["tech"]
        custnr = options["custnr"]

        func_add_trunk(tech, custnr)

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
