from datetime import date

from django.core.management.base import BaseCommand

from dit_enterprisehub_sap.models import *
from dit_enterprisehub_ucmgmt.models import *


class Command(BaseCommand):
    help = "Sync call routing"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        all_nums = (
            DolTelCusNum.objects.using("sapserver")
            .filter(u_portdate__lte=date.today())
            .filter(u_blockfrom__isnull=True)
            .filter(u_blockto__isnull=True)
        )

        for number in all_nums:
            try:
                routing_entry = CusDialplan.objects.using("mediasw_conf").get(
                    called_nr="+49" + str(number.u_onkz) + str(number.u_mainnum)
                )
            except CusDialplan.DoesNotExist:
                print(
                    "Routing is missing an entry: "
                    + "+49"
                    + str(number.u_onkz)
                    + str(number.u_mainnum)
                    + " for customer "
                    + str(number.u_custnr.cardname)
                )

                try:
                    trunk_entry = CusCustnr.objects.using("mediasw_conf").filter(
                        cusnr_sap=number.u_custnr.cardcode
                    )
                except CusCustnr.DoesNotExist:
                    print("Trunk is missing an entry: " + str(number.u_custnr.cardname))
                    continue

                trunk = trunk_entry.first()
                print("Trunk: " + trunk.trunk_name.id)

                new_routing = CusDialplan(
                    match_prio=0,
                    called_nr="+49" + str(number.u_onkz) + str(number.u_mainnum),
                    type=CusEnumDpType.objects.using("mediasw_conf").get(dp_type_id=1),
                    dest_trunk_or_nr=trunk.trunk_name.id,
                )

                new_routing.save(force_insert=True, using="mediasw_conf")

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
